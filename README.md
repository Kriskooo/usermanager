# User Manager

User Manager is a .NET Core application designed to manage users. It provides a set of RESTful APIs to create, read, update, and delete users.

## Features

- User registration
- User authentication
- CRUD operations for user management

## Requirements

The requirements for this service can be found at this [link](https://gist.github.com/dimitara/aa323fa2523a53c52ad67fc4f4a54cbd).

## Getting Started

To get a local copy up and running, follow these steps:

1. Clone the repository.
2. Navigate to the project directory.
3. Run `docker-compose up --build` to build the Docker images and start the containers.

## Stopping the Application

To stop the application and remove the Docker containers, use the following command:
1. Navigate to the project directory.
2. Run `docker-compose down`

## Accessing Swagger UI

Once the application is running, you can access the Swagger UI by this [link](http://localhost:8080/swagger)