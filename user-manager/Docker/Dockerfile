# Base image for the runtime environment (ASP.NET application)
FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
USER app
WORKDIR /app
EXPOSE 8080

# Build image with SDK for compiling and publishing the application
FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
# Install clang/zlib1g-dev dependencies for publishing to native
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    clang zlib1g-dev
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["user-manager.csproj", "."]
RUN dotnet restore "./user-manager.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "./user-manager.csproj" -c $BUILD_CONFIGURATION -o /app/build

# Create a publish image for the final application
FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./user-manager.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=true

# Final image for the runtime environment (with only necessary dependencies)
FROM mcr.microsoft.com/dotnet/runtime-deps:7.0 AS final
WORKDIR /app
EXPOSE 8080
COPY --from=publish /app/publish .
ENTRYPOINT ["./user-manager"]
