﻿using Microsoft.AspNetCore.Identity;

public class ApplicationUser : IdentityUser
{
    public byte[]? Avatar { get; set; }
    public string? WebsiteUrl { get; set; }
}