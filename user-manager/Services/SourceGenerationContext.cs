﻿using System.Text.Json.Serialization;
using user_manager.Api.Authentication;

namespace user_manager.Services;

[JsonSourceGenerationOptions(PropertyNamingPolicy = JsonKnownNamingPolicy.CamelCase, GenerationMode = JsonSourceGenerationMode.Default)]
[JsonSerializable(typeof(AuthenticationRequest))]
[JsonSerializable(typeof(AuthenticationResponse))]
[JsonSerializable(typeof(RegisterUser))]

internal partial class SourceGenerationContext : JsonSerializerContext
{
}