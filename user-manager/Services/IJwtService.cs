﻿using user_manager.Api.Authentication;

namespace user_manager.Services
{
    public interface IJwtService
    {
        AuthenticationResponse CreateToken(ApplicationUser user);
    }
}