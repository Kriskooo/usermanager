﻿namespace user_manager.Api.Profile
{
    public class UpdateProfileRequest
    {
        public IFormFile Avatar { get; set; }
        public string? WebsiteUrl { get; set; }
        public string? Email { get; set; }
        public string? Username { get; set; }
    }
}
