﻿using Microsoft.EntityFrameworkCore;
using user_manager.CommonExceptions;

namespace user_manager.Api.Profile;

public class ProfileService : IProfileService
{
    private readonly ApplicationDbContext _context;
    public ProfileService(ApplicationDbContext context)
    {
        _context = context;
    }
    public async Task<ProfileDto> GetProfileAsync(string username)
    {
        return await _context.Users
            .Where(applicationUser => applicationUser.UserName == username)
            .Select(applicationUser => new ProfileDto
            {
                Username = applicationUser.UserName,
                Avatar = applicationUser.Avatar != null ? Convert.ToBase64String(applicationUser.Avatar) : null,
                WebsiteUrl = applicationUser.WebsiteUrl
            })
            .FirstOrDefaultAsync() ?? throw new EntityNotFoundException("User not found!");
    }

    public async Task<List<ProfileDto>> QueryProfilesAsync()
    {
        return await _context.Users
            .Select(applicationUser => new ProfileDto
            {
                Username = applicationUser.UserName,
                Avatar = applicationUser.Avatar != null ? Convert.ToBase64String(applicationUser.Avatar) : null,
                WebsiteUrl = applicationUser.WebsiteUrl
            })
            .ToListAsync();
    }

    public async Task UpdateProfileAsync(UpdateProfileRequest request)
    {
        var user = await _context.Users.FirstOrDefaultAsync(u => u.UserName == request.Username)
            ?? throw new EntityNotFoundException("User not found!");

        using var memoryStream = new MemoryStream();
        await request.Avatar.CopyToAsync(memoryStream);

        user.Avatar = memoryStream.ToArray();
        user.WebsiteUrl = request.WebsiteUrl;
        user.Email = request.Email;
        user.UserName = request.Username;

        await _context.SaveChangesAsync();
    }
}