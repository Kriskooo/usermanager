﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace user_manager.Api.Profile
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class ProfilesController : ControllerBase
    {
        private readonly IProfileService _profileService;
        public ProfilesController(IProfileService profileService)
        {
            _profileService = profileService;
        }

        [HttpGet("{username}")]
        public async Task<IActionResult> GetProfile(string username)
        {
            var profile = await _profileService.GetProfileAsync(username);
            return Ok(profile);
        }

        [HttpGet]
        public async Task<IActionResult> QueryProfiles()
        {
            var profiles = await _profileService.QueryProfilesAsync();
            return Ok(profiles);
        }

        [HttpPut("update")]
        public async Task<IActionResult> UpdateProfile([FromForm] UpdateProfileRequest request)
        {
            await _profileService.UpdateProfileAsync(request);
            return NoContent();
        }
    }
}
