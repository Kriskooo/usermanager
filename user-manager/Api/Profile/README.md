# Profile Controller

The `ProfilesController` is responsible for managing user profiles. It uses the `IProfileService` to handle the profile management logic.

## Endpoints

### Get Profile
This endpoint allows retrieval of a user's profile by username. It expects a `GET` request to http://localhost:8080/api/profile/{username}.

Possible status codes:

- 200 OK: The profile was successfully retrieved.
- 404 Not Found: The profile does not exist.
- 401 Unauthorized: The JWT is invalid or expired.

### Query Profiles
This endpoint allows querying all profiles. It expects a `GET` request to http://localhost:8080/api/profile.

Possible status codes:

- 200 OK: The profiles were successfully retrieved.
- 401 Unauthorized: The JWT is invalid or expired.

### Update Profile
This endpoint allows a user to update their profile. It expects a `PUT` request to http://localhost:8080/api/profile with a JSON body containing the updated profile details.

Possible status codes:

- 204 No Content: The profile was successfully updated.
- 400 Bad Request: The update failed, the error details will be in the response body.
- 401 Unauthorized: The JWT is invalid or expired.
