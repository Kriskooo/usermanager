﻿namespace user_manager.Api.Profile
{
    public interface IProfileService
    {
        Task<ProfileDto> GetProfileAsync(string username);
        Task<List<ProfileDto>> QueryProfilesAsync();
        Task UpdateProfileAsync(UpdateProfileRequest request);
    }
}