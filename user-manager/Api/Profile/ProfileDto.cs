﻿namespace user_manager.Api.Profile
{
    public class ProfileDto
    {
        public string Username { get; set; }
        public string Avatar { get; set; }
        public string? WebsiteUrl { get; set; }
    }
}
