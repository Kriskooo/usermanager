﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace user_manager.Api.Authentication;

[Route("api/[controller]")]
[ApiController]
public class AuthenticationController : ControllerBase
{
    private readonly IAuthService _authService;
    public AuthenticationController(IAuthService authService)
    {
        _authService = authService;
    }

    [HttpPost("register")]
    public async Task<ActionResult> RegisterUserAsync(RegisterUser request)
    {
        IdentityResult? result = await _authService.RegisterUserAsync(request);

        if (!result.Succeeded)
        {
            return BadRequest(result.Errors);
        }

        return CreatedAtAction(null, null);
    }

    [HttpPost("login")]
    public async Task<ActionResult<AuthenticationResponse>> LoginAsync(AuthenticationRequest request)
    {
        var response = await _authService.LoginAsync(request);

        if (response == null)
        {
            return BadRequest("Bad credentials");
        }

        return Ok(response);
    }
}
