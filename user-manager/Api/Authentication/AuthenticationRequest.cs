﻿using System.ComponentModel.DataAnnotations;

namespace user_manager.Api.Authentication;

public class AuthenticationRequest
{
    [Required]
    public string UserName { get; set; }

    [Required]
    public string Password { get; set; }
}
