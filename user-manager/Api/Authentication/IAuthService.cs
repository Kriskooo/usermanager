﻿using Microsoft.AspNetCore.Identity;

namespace user_manager.Api.Authentication
{
    public interface IAuthService
    {
        Task<AuthenticationResponse> LoginAsync(AuthenticationRequest request);
        Task<IdentityResult> RegisterUserAsync(RegisterUser request);
    }
}