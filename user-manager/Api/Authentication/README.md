# Authentication Controller

The `AuthenticationController` is responsible for user registration and login. It uses the `IAuthService` to handle the authentication logic.

## Endpoints

### Register User

This endpoint allows a new user to register. It expects a `POST` request to `http://localhost:8080/api/authentication/register` with a JSON body containing the user details.

Example request:
POST http://localhost:8080/api/authentication/register HTTP/1.1
Content-Type: application/json

{
    "username": "newuser",
    "password": "password123"
}

Possible status codes:

- 201 Created: Registration was successful.
- 400 Bad Request: Registration failed, the error details will be in the response body.

### Login User

This endpoint allows a user to login. It expects a POST request to http://localhost:8080/api/authentication/login with a JSON body containing the username and password.

POST http://localhost:8080/api/authentication/login HTTP/1.1
Content-Type: application/json

{
    "username": "existinguser",
    "password": "password123"
}

Possible status codes:

- 200 OK: Login was successful, the authentication details will be in the response body.
- 400 Bad Request: Login failed, the error message "Bad credentials" will be in the response body.