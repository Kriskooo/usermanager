﻿namespace user_manager.Api.Highlights
{
    public class PlayerHighlight
    {
        public string? Player { get; set; }
        public double Value { get; set; }
    }
}
