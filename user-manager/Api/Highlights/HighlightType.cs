﻿namespace user_manager.Api.Highlights
{
    public enum HighlightType
    {
        MostCompletePercentage,
        LongDistancePass,
    }
}
