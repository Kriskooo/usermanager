﻿namespace user_manager.Api.Highlights
{
    public interface IHighlightsHandler
    {
        Dictionary<HighlightType, PlayerHighlight> GenerateHighlights(List<Pass> passes);
    }
}
