﻿namespace user_manager.Api.Highlights
{
    public class HighlightsHandler : IHighlightsHandler
    {
        private const string COMPLETE_STATUS = "complete";

        public Dictionary<HighlightType, PlayerHighlight> GenerateHighlights(List<Pass> passes)
        {
            Dictionary<string, Stats> playerStats = new Dictionary<string, Stats>();

            foreach (var pass in passes)
            {
                if (!playerStats.TryGetValue(pass.Receiver, out Stats? stats))
                {
                    stats = new Stats();
                    playerStats[pass.Receiver] = stats;
                }

                UpdateStats(pass, stats);
            }
            PlayerHighlight mostCompletePercentage = CalculateMostCompletePercentage(playerStats);
            PlayerHighlight longestPass = GetLongestDistancePass(playerStats);

            Dictionary<HighlightType, PlayerHighlight> highlights = new()
            {
                { HighlightType.MostCompletePercentage, mostCompletePercentage },
                { HighlightType.LongDistancePass, longestPass },
            };

            return highlights;
        }
        private static PlayerHighlight GetLongestDistancePass(Dictionary<string, Stats> playerStats)
        {
            var longestPassEntry = playerStats.MaxBy(p => p.Value.LongestPass);
            return new()
            {
                Player = longestPassEntry.Key,
                Value = longestPassEntry.Value.LongestPass
            };
        }

        private static PlayerHighlight CalculateMostCompletePercentage(Dictionary<string, Stats> playerStats) => playerStats
                .Select(mostCompletePercentageEntry => new PlayerHighlight
                {
                    Player = mostCompletePercentageEntry.Key,
                    Value = (double)mostCompletePercentageEntry.Value.Completions / mostCompletePercentageEntry.Value.Total * 100
                })
                .MaxBy(p => p.Value);

        private static void UpdateStats(Pass pass, Stats stats)
        {
            stats.Completions += pass.Result == COMPLETE_STATUS ? 1 : 0;
            stats.Total++;
            stats.TotalDistance += pass.Distance;
            stats.LongestPass = Math.Max(stats.LongestPass, pass.Distance);
        }
    }
}
