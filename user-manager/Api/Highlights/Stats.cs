﻿namespace user_manager.Api.Highlights
{
    public class Stats
    {
        public int Completions { get; set; }
        public int Total { get; set; }
        public double TotalDistance { get; set; }
        public double LongestPass { get; set; }
    }
}
