﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace user_manager.Api.Highlights
{
    [Route("api/[controller]")]
    public class HighlightsController : ControllerBase
    {
        private readonly IHighlightsHandler _highlightsHandler;

        public HighlightsController(IHighlightsHandler highlightsHandler)
        {
            _highlightsHandler = highlightsHandler;
        }

        [HttpPost]
        public ActionResult<Dictionary<HighlightType, PlayerHighlight>> GetHighlights([FromBody] List<Pass> passes)
        {
            if (passes.IsNullOrEmpty())
            {
                return BadRequest("Invalid input. The list of passes cannot be null or empty.");
            }

            try
            {
                var highlights = _highlightsHandler.GenerateHighlights(passes);
                return Ok(highlights);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
