﻿using System.ComponentModel.DataAnnotations;

namespace user_manager.Api.Highlights
{
    public class Pass
    {
        [Required]
        public string Receiver { get; set; }

        [Required]
        public string Result { get; set; }

        [Range(0, double.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public double Distance { get; set; }
    }
}
